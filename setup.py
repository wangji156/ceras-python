
import setuptools

with open("README.txt", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ceras",
    version="0.0.1",
    author="17RADIX",
    author_email="17raidx@gmail.com",
    description="Quick plugin framework building system",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://www.ceras.net",
    license = 'MIT License',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points = {
        'console_scripts': ['ceras-master=ceras.command:command_line'],
    }
)