# CERAS

CERAS 灵感来源于JAVE OSGI的动态bundle思想，以及wordpress等PHP应用程序的拓展点思想

旨在提供一套为已开发好的应用程序提供轻量级的插件特性，以及为新项目提供插件化程序开发流程

# 简介

版本为开发中状态

CERAS-PYTHON提供基于python的插件化体系开发方法，包含

插件化框架工程创建与维护功能

插件包工程创建与维护功能

CERAS实用工具

# 入门
# python 项目集成指南
# Bundle和Package

Bundle定义为可运行插件包，Bundle具有生命周期特性，可以安装部署卸载等功能，每个Bundle拥有自己的工作区间

Package为Bundle暴露提供的使用接口（第二个接口为拓展点系统），存于Bundle内部，在Python项目中为一个moudle，每个bundle包括多个Package，bundle之间的依赖主要通过Package支撑

# 拓展点系统

如果你的系统存在可以拓展的地方，比如某个UI列表，里面可以插入更多视图的地方，或者说增加一种新类型的功能，就好比汽车上面的面板多一个立体声音响功能一样。

通过已有的拓展点和现有的拓展点系统对接

# 日志和异常
# 国际化
# 前端插件化方案与CERAS

我们提供了基于Angular懒加载的插件方案以及React和标准ES6代码的插件解决方案，需要特殊的插件工程。

请参阅：


