import os
template_path = os.path.dirname(os.path.abspath(__file__))
framework_template_path =  os.path.join(template_path,'framework')
bundle_template_path = os.path.join(template_path,'bundle')
framework_configfile_template_path =  os.path.join(framework_template_path,'ceras_config.py')
framework_datadir_template_path =  os.path.join(framework_template_path,'ceras_data')
framework_libfile_template_path =  os.path.join(framework_template_path,'ceraslib.py')