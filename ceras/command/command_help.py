import os
from ceras.command.common import getargs,print_line

MainHelpText='''CERAS-PYTHON 0.0.1
usage: python [subcommand] [option] [arg] [environment]
subcommand:
new     Create a new project
help    Get help
'''

def command_help(options,args):
    print_line(MainHelpText)