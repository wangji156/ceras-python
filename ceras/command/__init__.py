import os
from ceras.command.ceras_console_master import CerasConsoleMaster
def command_line(argv=None):
    master=CerasConsoleMaster(argv)
    master.execute()
