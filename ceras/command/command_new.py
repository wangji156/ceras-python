import shutil
from ceras.template import bundle_template_path,framework_template_path
from ceras.command.common import getargs,print_line
import os
import sys
def command_new(options,args):
    try:
        args0 = getargs(options.args,0)
        args1 = getargs(options.args,1)
        target = os.getcwd()
        if args1 == None:
            args1 = "new_ceras_project"
        target = os.path.join(target,args1)
        if os.path.exists(target):
            print_line("please give new dir")
            return False
        if args0 == "framework":
            shutil.copytree(framework_template_path,target)
            print_line ("that is ok")
        else:
            print_line("help")
    except IndexError:
            print_line("help")