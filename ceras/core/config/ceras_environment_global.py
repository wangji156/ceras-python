from ceras.core.common import path_deal_with_sep
import os
from ceras.core.config import CerasConfigGlobal


class CerasEnvironmentGlobal(object):
    
    data_path = ""
    data_bundles_path = ""
    data_generate_path = ""
    data_original_bundles_path = ""
    data_original_workspace_path = ""
    data_relative_path = ""
    data_bundles_relative_path = ""
    data_generate_relative_path = ""
    data_original_bundles_relative_path = ""
    data_original_workspace_relative_path = ""
    @staticmethod
    def get_data_relative_path():
        return path_deal_with_sep(CerasConfigGlobal.data_relative_path)
    @staticmethod
    def get_data_bundles_relative_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_relative_path(),"bundles")

    @staticmethod
    def get_data_generate_relative_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_relative_path(),"generate")

    @staticmethod
    def get_data_original_bundles_relative_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_relative_path(),"original_bundles")

    @staticmethod
    def get_data_original_workspace_relative_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_relative_path(),"workspace")
    @staticmethod
    def get_data_path():
        cwd = os.getcwd()
        arr = path_deal_with_sep(CerasConfigGlobal.data_relative_path,'.').split('.')
        relative_path = cwd
        for cur in arr:
            relative_path = os.path.join(relative_path, cur)
        return relative_path

    @staticmethod
    def get_data_bundles_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_path(),"bundles")

    @staticmethod
    def get_data_generate_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_path(),"generate")

    @staticmethod
    def get_data_original_bundles_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_path(),"original_bundles")

    @staticmethod
    def get_data_original_workspace_path():
        return os.path.join(CerasEnvironmentGlobal.get_data_path(),"workspace")

CerasEnvironmentGlobal.data_path = CerasEnvironmentGlobal.get_data_path()
CerasEnvironmentGlobal.data_bundles_path = CerasEnvironmentGlobal.get_data_bundles_path()
CerasEnvironmentGlobal.data_generate_path = CerasEnvironmentGlobal.get_data_generate_path()
CerasEnvironmentGlobal.data_original_bundles_path = CerasEnvironmentGlobal.get_data_original_bundles_path()
CerasEnvironmentGlobal.data_original_workspace_path = CerasEnvironmentGlobal.get_data_original_workspace_path()
CerasEnvironmentGlobal.data_relative_path = CerasEnvironmentGlobal.get_data_relative_path()
CerasEnvironmentGlobal.data_bundles_relative_path = CerasEnvironmentGlobal.get_data_bundles_relative_path()
CerasEnvironmentGlobal.data_generate_relative_path = CerasEnvironmentGlobal.get_data_generate_relative_path()
CerasEnvironmentGlobal.data_original_bundles_relative_path = CerasEnvironmentGlobal.get_data_original_bundles_relative_path()
CerasEnvironmentGlobal.data_original_workspace_relative_path = CerasEnvironmentGlobal.get_data_original_workspace_relative_path()


