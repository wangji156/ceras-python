from ceras.core.config import CerasConfigGlobal, CerasEnvironmentGlobal
from ceras.core.common import path_deal_with_sep
import importlib
import os


class CerasGenerateGlobal:
    bundles_packages_relative_index = ""
    bundles_packages_relative_path = ""
    bundles_packages_content = None

    @staticmethod
    def get_bundles_packages_relative_index():
        indextemp = os.path.join(
            CerasEnvironmentGlobal.data_generate_relative_path, "bundles_packages")
        return path_deal_with_sep(indextemp, '.')

    @staticmethod
    def get_bundles_packages_relative_path():
        return os.path.join(CerasEnvironmentGlobal.data_generate_relative_path, "bundles_packages.py")

    @staticmethod
    def get_bundles_packages_content():
        content = importlib.import_module(
            CerasGenerateGlobal.bundles_packages_relative_index)
        return content

CerasGenerateGlobal.bundles_packages_relative_index = CerasGenerateGlobal.get_bundles_packages_relative_index()
CerasGenerateGlobal.bundles_packages_relative_path = CerasGenerateGlobal.get_bundles_packages_relative_path()
CerasGenerateGlobal.bundles_packages_content = CerasGenerateGlobal.get_bundles_packages_content()
