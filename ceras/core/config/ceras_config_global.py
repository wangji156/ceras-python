try:
    cerasconfigmodle = __import__("ceras_config")
except:
    cerasconfigmodle = None
    

class CerasConfigGlobal(object):

    data_relative_path = ""

    @staticmethod
    def get_data_relative_path() -> str:
        ceras_data_path = "ceras_data"
        try:
            ceras_data_path = cerasconfigmodle.ceras_data_path
        except:
            ceras_data_path = "ceras_data"
        return ceras_data_path

CerasConfigGlobal.data_relative_path = CerasConfigGlobal.get_data_relative_path()


    
    