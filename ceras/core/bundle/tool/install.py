import os
import zipfile
from shutil import copyfile
from ceras.core.common import path_deal_with_sep
from ceras import CerasConfigGlobal,CerasEnvironmentGlobal

def install_bundle_by_path(path):
    
    pass

def _get_symbol_name(name,version):
    return name + "~" + version

def _get_bundle_path(symbol):
    return os.path.join(CerasEnvironmentGlobal.data_bundles_path,symbol)

def _get_bundle_jsonfile_path(symbol):
    bundlepath = _get_bundle_path(symbol)
    return os.path.join(bundlepath,"bundle.json")

def _un_zip(fromfile,todic):
    zip_ref = zipfile.ZipFile(fromfile, 'r')
    zip_ref.extractall(todic)
    zip_ref.close()
