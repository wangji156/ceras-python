from enum import Enum, unique
@unique
class LanguageType(Enum):
    zh_CN = "zh_CN"
    en_US = "en_US"
