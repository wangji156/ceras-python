from ceras.core.log.model import LogType

class CerasBundleLogger():

    def debug(self, msg, *args, **kwargs):
        self.log(LogType.DEBUG, msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.log(LogType.INFO, msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self.log(LogType.WARNING, msg, *args, **kwargs)

    def warn(self, msg, *args, **kwargs):
        self.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.log(LogType.ERROR, msg, *args, **kwargs)

    def exception(self, msg, *args, exc_info=True, **kwargs):
        self.log(LogType.ERROR, msg, *args, exc_info=exc_info, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.log(LogType.CRITICAL, msg, *args, **kwargs)

    def log(self, level, msg, *args, **kwargs):
        pass
